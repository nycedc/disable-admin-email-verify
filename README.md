# Disable Admin Email Verification

## Description
This is a WordPress plugin that is meant to disable the [Administrator Email check](https://make.wordpress.org/core/2019/10/17/wordpress-5-3-admin-email-verification-screen/) that was started in release 5.3.

### Why disable it?
Because it messes up my automated tests.

### Why not just add this functionality to a site's functions.php file?
Because most WordPress sites are hacky enough as it is! At my job I manage a lot of WordPress sites and the last thing I want to do is add a hard-coded hack to each one, this plugin allows me to safely test each site in an automated fashion.

## Installation
This plugin is meant to be installed via [Composer](https://getcomposer.org/). For details on how Composer can be used with Wordpress see this [documentation](https://wpackagist.org/).

## Special Thanks
The code to actually disable the check was taken from this [blog post](https://mainwp.com/how-to-disable-admin-email-verification-on-your-child-sites/).
